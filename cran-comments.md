## Test environments

-  local ubuntu 20.04 R 4.2.1
-  win-builder (devel and release)

## R CMD check results

There were no ERRORs or WARNINGs.

There was 1 NOTE:

-installed size is 11.5Mb
sub-directories of 1Mb or more:
libs   9.8Mb

This is because of documentation figures. Since HBV.IANIGLA is a complex package that really needs elaborated examples, I decided to incorporate some figures in order to ease function usability.
