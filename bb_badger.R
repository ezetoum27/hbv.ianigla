#********************
#* badger developer
#********************
#* source: https://github.com/GuangchuangYu/badger

#* library
library(badger)


#* Package Version
badge_cran_release("HBV.IANIGLA", "orange")

# CRAN checks results
badge_cran_checks("HBV.IANIGLA")

#* Download stats for CRAN
badge_cran_download("HBV.IANIGLA", "last-month", "green")

#* Development
  # Lifecycle
badge_lifecycle("maturing", "blue")

  # License
badge_license(color = 'forestgreen')

  # coveralls code coverage
# https://walczak.org/2017/06/how-to-add-code-coverage-codecov-to-your-r-package/

  # last commit date






