#include <Rcpp.h>
#ifndef DO_TAIR_GRADIENT
#define DO_TAIR_GRADIENT

Rcpp::List do_tair_gradient(Rcpp::NumericVector v_tair,
                            double station_alt,
                            Rcpp::NumericVector v_altitude,
                            Rcpp::NumericVector v_grad);
#endif
