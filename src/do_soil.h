#include <Rcpp.h>
#ifndef DO_SOIL
#define DO_SOIL

Rcpp::List do_soil(Rcpp::NumericMatrix m_qflow,
                   Rcpp::NumericMatrix m_pet,
                   Rcpp::NumericMatrix m_swe,
                   double swc,
                   Rcpp::NumericMatrix m_param);
#endif
