#include <Rcpp.h>
#ifndef DO_TRANSFER
#define DO_TRANSFER

Rcpp::NumericVector do_transfer(int model,
                                Rcpp::NumericVector v_qflow,
                                Rcpp::NumericVector v_param);
#endif
