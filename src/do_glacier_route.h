#include <Rcpp.h>
#ifndef DO_GLACIER_ROUTE
#define DO_GLACIER_ROUTE

Rcpp::NumericMatrix do_glacier_route(int model,
                                     Rcpp::NumericMatrix m_input,
                                     double init_cond,
                                     Rcpp::NumericVector v_param);
#endif
