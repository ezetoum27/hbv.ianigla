#include <Rcpp.h>
#ifndef DO_RESCALE
#define DO_RESCALE

Rcpp::NumericMatrix do_rescale(Rcpp::List x,
                               Rcpp::NumericVector area);
#endif
