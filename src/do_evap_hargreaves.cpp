#include <Rcpp.h>
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_evap_hargreaves
//'
//' @title Hargreaves reference evapotranspiration
//'
//' @description Computes crop reference evapotranspiration
//' using Hargreaves equation (Hargreaves and Allen, 2003).
//'
//' @usage do_evap_hargreaves(
//'        m_tmean,
//'        m_tmax,
//'        m_tmin,
//'        m_kin
//'        )
//'
//' @param m_tmean numeric matrix with daily mean air temperature
//' \eqn{[°C]} (\code{ncol = number of zones}).
//'
//' @param m_tmax numeric matrix with daily maximum air temperature
//' \eqn{[°C]} (\code{ncol = number of zones}).
//'
//' @param m_tmin numeric matrix with daily minimum air temperature
//' \eqn{[°C]} (\code{ncol = number of zones}).
//'
//' @param m_kin numeric matrix with daily total incoming
//' extraterrestrial solar radiation \eqn{[MJ/(m^2 day)]}
//' (\code{ncol = number of zones}).
//'
//' @note The equation used by the model is:
//'
//' \eqn{ET_o = 0.0023 * (T_{mean} + 17.8) * (T_{max} - T_{min})^{0.5} * Ra * 0.408}
//'
//' if the temperature value is below \eqn{0°C}, the model sets it to
//' \eqn{0°C}. The \eqn{0.408} value is to convert \eqn{[MJ/(m^2 day)]}
//' to \eqn{[mm/day]}.
//'
//' @return List object with the following numeric matrices
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{evap_ref} \eqn{[mm/day]}.
//' }
//'
//' @export
//'
// [[Rcpp::export]]
List do_evap_hargreaves(NumericMatrix m_tmean,
                        NumericMatrix m_tmax,
                        NumericMatrix m_tmin,
                        NumericMatrix m_kin){
   // iteraciones = nzones = columnas matrices
   int n_it = m_tmean.ncol();

   // filas
   int n = m_tmean.nrow();

   // matrices de salida
   NumericMatrix m_evap_ref(n, n_it);


   for (int i = 0; i < n_it; ++i){
     // itero sobre columnas

     for(int j = 0; j < n; ++j){
       // itero sobre filas

       double tmean = std::max( m_tmean(j, i), 0.0);
       double tmax  = std::max( m_tmax(j, i), 0.0);
       double tmin  = std::max( m_tmin(j, i), 0.0);

       double kin   = m_kin(j, i);

       m_evap_ref(j, i) =
         0.0023 * ( tmean + 17.8 ) * pow( ( tmax - tmin ), 0.5) * kin * 0.408;

     }



   }

   // salida
   List evap_out =
     List::create(Named("evap_ref") = m_evap_ref);

   return evap_out;

 }
