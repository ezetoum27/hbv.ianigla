#include <Rcpp.h>
#include "aa_snowmelt.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_snow
//'
//' @title Snow accumulation and melt routine
//'
//' @description Allows you to simulate snow accumulation
//' and melting processes using a temperature index approach.
//'
//' @usage do_snow(
//'        m_tair,
//'        m_rainfall,
//'        m_snowfall,
//'        swe_init,
//'        m_param
//'        )
//'
//' @param m_tair numeric matrix with air temperatures
//' \eqn{[°C/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param m_rainfall numeric matrix with rainfall
//' \eqn{[mm/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param m_snowfall numeric matrix with snowfall
//' \eqn{[mm/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param swe_init double with initial snow water equivalent
//' \eqn{[mm]}.
//'
//' @param m_param numeric matrix with parameters
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{SFCF}: snowfall correction factor \eqn{[-]}.
//'  \item \code{Tt}: melt temperature \eqn{[ºC]}.
//'  \item \code{fm}: snowmelt factor \eqn{[mm/°C.\Delta t]}.
//'}
//'
//' @return List object with the following numeric matrices
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{rainfall} \eqn{[mm/\Delta t]}.
//'  \item \code{snowfall} \eqn{[mm/\Delta t]}.
//'  \item \code{swe} \eqn{[mm/\Delta t]}.
//'  \item \code{melt} \eqn{[mm/\Delta t]}.
//'  \item \code{qflow}: \code{rainfall + melt} \eqn{[mm/\Delta t]}.
//' }
//'
//' @export
//'
// [[Rcpp::export]]
List do_snow(NumericMatrix m_tair,
             NumericMatrix m_rainfall,
             NumericMatrix m_snowfall,
             double swe_init,
             NumericMatrix m_param){

  // iteraciones = nzones = columnas matrices
  int n_it = m_tair.ncol();

  // filas
  int n = m_tair.nrow();

  // matrices de salida
  NumericMatrix m_rain(n, n_it);
  NumericMatrix m_snow(n, n_it);
  NumericMatrix m_swe(n, n_it);
  NumericMatrix m_melt(n, n_it);
  NumericMatrix m_qflow(n, n_it);

  // itero sobre las zonas
  for (int i = 0; i < n_it; ++i){

    // elementos de entrada
    NumericMatrix m_input(n, 3);
    NumericVector v_tair = m_tair(_, i);
    NumericVector v_rain = m_rainfall(_, i);
    NumericVector v_snow = m_snowfall(_, i);

    NumericVector v_init_cond =
      NumericVector::create(swe_init, 2); // es de tamaño dos por herencia

    NumericVector v_param = m_param(_, i);


    // asigno
    m_input(_, 0) = v_tair;
    m_input(_, 1) = v_rain;
    m_input(_, 2) = v_snow;


    // rutina de nieve
    NumericMatrix m_snow_model =
      snowmelt_pro(m_input,
                   v_init_cond,
                   v_param);

    // asigno columnas a la matriz general
    m_rain(_, i)  = m_snow_model(_, 0);
    m_snow(_, i)  = m_snow_model(_, 1);
    m_swe(_, i)   = m_snow_model(_, 2);
    m_melt(_, i)  = m_snow_model(_, 3);
    m_qflow(_, i) = m_snow_model(_, 4);



  }

  // salida
  List snow_out =
    List::create(Named("rainfall") = m_rain,
                 Named("snowfall") = m_snow,
                 Named("swe")      = m_swe,
                 Named("melt")     = m_melt,
                 Named("qflow")    = m_qflow );

  return snow_out;

}
