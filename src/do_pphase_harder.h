#include <Rcpp.h>
#ifndef DO_PPHASE_HARDER
#define DO_PPHASE_HARDER

Rcpp::List do_pphase_harder(Rcpp::NumericMatrix m_tair,
                            Rcpp::NumericMatrix m_rh,
                            Rcpp::NumericMatrix m_precip,
                            Rcpp::String period = "1 day");
#endif
