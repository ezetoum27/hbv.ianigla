#include <Rcpp.h>
#ifndef DO_EVAP_HARGREAVES
#define DO_EVAP_HARGREAVES

Rcpp::List do_evap_hargreaves(Rcpp::NumericMatrix m_tmean,
                              Rcpp::NumericMatrix m_tmax,
                              Rcpp::NumericMatrix m_tmin,
                              Rcpp::NumericMatrix m_kin);
#endif
