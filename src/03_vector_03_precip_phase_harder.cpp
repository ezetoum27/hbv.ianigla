#include <Rcpp.h>
#include "02_single_10_precip_phase_harder.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++
//  hydroflex package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++

// INTERNAL
// SOURCE: Harder and Pomeroy (2013)

//' @name v_precip_phase_harder
//'
//' @title Precipitation phase - Harder and Pomeroy (2013)
//'
//' @description Estimate precipitation phase partioning
//' based on Tair and RH (vectorised version).
//'
//' @param tair numeric vector with recorded
//' air temperature \eqn{[ºC]}.
//' @param rh numeric vector with relative humidity \eqn{[\%]}.
//' @param delta_x double with increment to derivate using
//' the limit's concept.
//' @param error_thres double with absolute error. This
//' value is used as a convergence criteria in the
//' Newton-Raphson (secant) method.
//' @param period character with:
//' * "15 min" => 1
//' * "1 hour" => 2
//' * "1 day"  => 3
//'
//' @return A numeric matrix with two columns:
//' * 1. the snowfall fraction \eqn{[-]}
//' * 2. the rainfall fraction \eqn{[-]}


// [[Rcpp::export]]
NumericMatrix v_precip_phase_harder(NumericVector tair,
                                    NumericVector rh,
                                    double delta_x = 0.01,
                                    double error_thres = 0.01,
                                    String period = "1 day"){
  int n_it = tair.length();
  NumericMatrix harder_out(n_it, 2);

  int periodo;

  if(period == "1 hour"){
    periodo = 2;

  } else if(period == "15 min"){
    periodo = 1;

  } else if(period == "1 day"){
    periodo = 3;
  }


  for(int i = 0; i < n_it; i++){
    NumericVector p_phase =
      precip_phase_harder_secant(tair(i),
                                 rh(i),
                                 delta_x,
                                 error_thres,
                                 periodo);

    harder_out(i, 0) = p_phase(0); // fsnow
    harder_out(i, 1) = p_phase(1); // frain

  }

  return harder_out;
}
