#include <Rcpp.h>
#include "01_constant_physics.h"
#include "02_single_02_e_sat.h"
using namespace Rcpp;

// **********************************************************
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
//  **********************************************************
//  hydroflex package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
//  **********************************************************

// INTERNAL
// SOURCE: Harder y Pomeroy (2013)


//' @name precip_phase_harder_secant
//'
//' @title Precipitation phase - Harder and Pomeroy (2013)
//'
//' @description Estimate precipitation phase partioning
//' based on Tair and RH
//'
//' @param tair recorded air temperature \eqn{[ºC]}.
//' @param rh relative humidity \eqn{[\%]}.
//' @param error_thres double with absolute error. This
//' value is used as a convergence criteria in the
//' Newton-Raphson method.
//' @param period integer value (1, 2 or 3), where:
//' * 1 => 15 min record period
//' * 2 => 1 hour record period
//' * 3 => 1 day record period
//'
//' @return A numeric vector with:
//' * the snowfall fraction \eqn{[-]}
//' * the rainfall fraction \eqn{[-]}
//' * hydrometeor temperature \eqn{[ºC]}
//' * the required number of iterations \eqn{[-]}


// [[Rcpp::export]]
NumericVector precip_phase_harder_secant(double tair,
                                         double rh,
                                         double delta_x,
                                         double error_thres,
                                         int period){

  double tair_k = tair + 273.15;

  //++++++++++++++++++++++++++++++
  // definimos y luego calculamos:
  //++++++++++++++++++++++++++++++
  // D_e: difusividad del vapor de agua en el aire [m2 / s]
  // lambda_air: conductividad térmica del aire [J / (m * s * K)]
  // lambda_any: calor latente de sublimación o vaporización [MJ / kg]
  // e_vap: presión de vapor de agua en el ambiente [kPa]
  // e_ast: presión de vapor de agua condiciones saturadas [kPa]
  // rho_vap: densidad del vapor de agua [kg / m3]

  double D_e, lambda_air, lambda_any, e_vap, e_ast, rho_vap;

  // Thorpe y Mason (1966)
  D_e = 2.06 * ( 1 / pow(10, 5) ) * pow( tair_k / 273.15, 1.75 ); // [m2 / s]

  // List (1949)
  lambda_air = 0.000063 * tair_k + 0.00673; // [J / (m * s * K)]

  // Rogers and Yau
  if(tair < 0.0){
    // calor latente de sublimación
    lambda_any = lambda_f() + lambda_v(tair); // [MJ / kg]

  } else {
    // calor latente de vaporización
    lambda_any = lambda_v(tair); // [MJ / kg]
  }

  e_ast = e_sat(tair);
  e_vap = (rh / 100) * e_ast;

  rho_vap = m_water() * e_vap * 1000 / ( R_gas() * tair_k ); // [kg/m3]

  //++++++++++++++++++++++++++++++
  // obtengo las ctes a, b y c
  //++++++++++++++++++++++++++++++
  // estos valores se corresponden
  // con lo que permanece invariante en
  // la ecuación de Ti
  // Ti = a + b * [c + rho_ti] - harder y pomeroy (2013)

  double a, b, c;

  a = tair_k; // [K]
  b = D_e * lambda_any * pow(10, 6) / lambda_air; // [m3 * K / kg]
  c = rho_vap; // [kg / m3]

  //++++++++++++++++++++++++++++++
  // comienzo a iterar
  //++++++++++++++++++++++++++++++
  double ti_init_k, ti_init, ti_new_k;
  double abs_error;

  double f_x, f_x_delta;
  double t_guess_k, t_guess_delta_k;
  double D_f;

  // valores iniciales
  ti_init_k = 273.15;
  ti_init   = 0.0;
  abs_error = 1.0;

  int i = 0;

  while (abs_error > error_thres){

    //++1 calcule f(xi) y f(xi + delta_x)
    t_guess_k = a + b * (c - m_water() * e_sat(ti_init) * 1000 /
      ( R_gas() * ti_init_k ) ); // [K]

    t_guess_delta_k = a + b * (c - m_water() * e_sat(ti_init + delta_x) *
      1000 /  ( R_gas() * (ti_init_k + delta_x) ) ); // [K]

    f_x = ti_init_k - t_guess_k; // [K o °C]

    f_x_delta = (ti_init_k + delta_x) - t_guess_delta_k; // [K o °C]

    //++2 aproximo la derivada
    D_f = (f_x_delta - f_x) / delta_x;

    //++3 calculo el nuevo valor y el error
    ti_new_k = ti_init_k - f_x / D_f; // [K]

    abs_error = std::abs(ti_new_k - ti_init_k);

    //++4 actualizo valores
    ti_init_k = ti_new_k; // [K]
    ti_init   = ti_init_k - 273.15; // [°C]

    i += 1;

    if (i == 200){
      break;
    }


  } // fin while()

  //+++++++++++++++++++++++++++++++++++
  // finalmente calculo las fracciones
  //+++++++++++++++++++++++++++++++++++
  double coef_b, coef_c;
  double f_rain, f_snow;

  if(period == 1){
    // 15 min
    coef_b = 2.630006;
    coef_c = 0.09336;

  } else if( period == 2){
    // 1 hour
    coef_b = 2.50286;
    coef_c = 0.125006;

  } else if( period == 3){
    // 1 day
    coef_b = 2.799856;
    coef_c = 0.293292;
  }

  f_rain = 1 / ( 1 + coef_b * pow( coef_c, ti_init ) );
  f_snow = 1.0 - f_rain;


  NumericVector precip_phase =
    NumericVector::create(Named("f_snowfall", f_snow),
                          Named("f_rainfall", f_rain),
                          Named("Ti", ti_init),
                          Named("n_it", i),
                          Named("abs_error", abs_error));

  return round( precip_phase, 2 );

}
