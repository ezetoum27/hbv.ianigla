#include <Rcpp.h>
#include "02_single_02_e_sat.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_rh_gradient
//'
//' @title Relative humidity extrapolation model
//'
//' @description Computes relative humidity using an extrapolation
//' linear model.
//'
//' @usage do_rh_gradient(
//'        v_tair,
//'        v_rh,
//'        station_alt,
//'        v_altitude,
//'        v_grad,
//'        option = "e_vap"
//'        )
//'
//' @param v_tair numeric vector with air temperatures series
//' \eqn{[°C]}.
//'
//' @param v_rh numeric vector with relative humidity series
//' \eqn{[\%]}.
//'
//' @param station_alt altitude of the air temperature and
//' relative humidity stations \eqn{[masl]}.
//'
//' @param v_altitude numeric vector with target altitudes
//' \eqn{[masl]} (\code{length = number of zones}).
//'
//' @param v_grad numeric vector with the following elements:
//' \enumerate{
//'
//'   \item air temperature linear gradient [ºC/km].
//'   E.g.: \eqn{-6.5}.
//'
//'   \item threshold height. Air temperature does not decrease
//'   when the altitude is higher than this value \eqn{[masl]}.
//' }
//'
//'
//' @param option string with:
//' \enumerate{
//'
//'   \item \code{"e_vap"}: assumes that vapour pressure is
//'   constant over all altitudes (\code{v_altitude}). This
//'   value will be constrain to \code{rh = 100 \%}.
//'
//'   \item \code{"rh"}: assumes constant relative humidity over
//'   all altitudes (\code{v_altitude}).
//'
//'
//' }
//'
//' @return List with extrapolated relative humidity
//' numeric matrix with (\code{ncol = number of zones}).
//'
//' @export
//'
// [[Rcpp::export]]
List do_rh_gradient(NumericVector v_tair,
                    NumericVector v_rh,
                    double station_alt,
                    NumericVector v_altitude,
                    NumericVector v_grad,
                    String option = "e_vap"){

 int n_row = v_tair.size();
 int n_col = v_altitude.size();

 NumericMatrix m_tair(n_row, n_col);
 NumericMatrix m_rh(n_row, n_col);

 double grad_tair  = v_grad(0);
 double grad_thres = v_grad(1); // altitudinal threshold

 if(option == "e_vap"){

   for(int i = 0; i < n_col; ++i){


     for(int j = 0; j < n_row; ++j){

       if(v_altitude(i) < grad_thres){

         m_tair(j, i) = (v_altitude(i) - station_alt) * (grad_tair / 1000) +
           v_tair(j);


       } else {

         m_tair(j, i) = (grad_thres - station_alt) * (grad_tair / 1000) +
           v_tair(j);
       }

       // saturados
       double e_ast = e_sat( m_tair(j, i) ); // altura en cuestión
       double e_ref = e_sat( v_tair(j) );   // medición

       double e_vap = ( v_rh(j) / 100) * e_ref;

       m_rh(j, i) = std::min(100.0, e_vap / e_ast * 100);


     }


   }

 } else if(option == "rh"){

   for(int j = 0; j < n_row; j++){

     NumericVector v(n_col, v_rh(j));

     m_rh(j, _) = v;

   }

 } else{

   stop("option argument not available");

 }



 // salida
 List rh_out =
   List::create(Named("rh") = m_rh);

 return rh_out;





}
