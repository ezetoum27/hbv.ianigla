#include <Rcpp.h>
#ifndef SOIL_HBV
#define SOIL_HBV

Rcpp::NumericMatrix Soil_HBV(int model,
                             Rcpp::NumericMatrix inputData,
                             Rcpp::NumericVector initCond,
                             Rcpp::NumericVector param);
#endif
