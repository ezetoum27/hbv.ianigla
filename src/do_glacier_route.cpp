#include <Rcpp.h>
#include "Glacier_Disch.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_glacier_route
//'
//' @title Glacier routing bucket function
//'
//' @description Implement a conceptual water storage and
//' release formulation for glacier runoff
//' routing (\code{\link{Glacier_Disch}}).
//'
//' @usage do_glacier_route(
//'        model,
//'        m_input,
//'        init_cond,
//'        v_param
//'        )
//'
//' @param model numeric integer with the model's choice.
//' The current HBV.IANIGLA version only supports
//' the S08 approach (see \code{\link{Glacier_Disch}}).
//'
//' @param m_input numeric matrix with two columns:
//' \strong{Model 1}
//'  \itemize{
//'  \item \code{column_1}: snow water equivalent above
//'  the glacier \eqn{[mm/\Delta t]}.
//'  The series can be obtained from the \code{\link{do_glacier}}
//'  function previously rescaled (\code{\link{do_rescale}}).
//'  \item \code{column_2}:  melted snow + melted ice + rainfall
//'  \eqn{[mm/\Delta t]}.
//'  The series can be obtained from the \code{\link{do_glacier}}
//'  function previously rescaled (\code{\link{do_rescale}}).
//'  }
//'
//' @param init_cond numeric value with the initial glacier reservoir
//' water content \strong{\code{SG}} \eqn{[mm]}.
//'
//' @param v_param numeric vector with the following values:
//'
//' \strong{Model 1 (S08)}
//'  \itemize{
//'  \item \code{KGmin}: minimal outflow rate \eqn{[1/\Delta t]}.
//'  \item \code{dKG}:  maximum outflow rate increase \eqn{[1/\Delta t]}.
//'  \item \code{AG}: scale factor \eqn{[mm]}.
//'  }
//'
//' @return Numeric matrix with the following columns:
//'
//' \strong{Model 1 (S08)}
//' \itemize{
//'   \item \code{Q}: glacier discharge \eqn{[mm/\Delta t]}.
//'   \item \code{SG}: glacier's bucket water storage
//'   content series \eqn{[1/\Delta t]}.
//' }
//'
//' @export
//'
// [[Rcpp::export]]
NumericMatrix do_glacier_route(int model,
                               NumericMatrix m_input,
                               double init_cond,
                               NumericVector v_param){

 NumericMatrix route_output =
    Glacier_Disch(model,
                  m_input,
                  init_cond,
                  v_param);

  return route_output;


}
