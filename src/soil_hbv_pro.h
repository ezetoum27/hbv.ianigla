#include <Rcpp.h>
#ifndef SOIL_HBV_PRO
#define SOIL_HBV_PRO

Rcpp::NumericMatrix soil_hbv_pro(Rcpp::NumericMatrix inputData,
                                 Rcpp::NumericVector initCond,
                                 Rcpp::NumericVector param);
#endif
