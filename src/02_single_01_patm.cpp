#include <Rcpp.h>
using namespace Rcpp;

// **********************************************************
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
//  **********************************************************
//  hydroflex package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
//  **********************************************************

// INTERNAL
// SOURCE: Dingman (2014)

//' @name p_atm
//' 
//' @title Atmospheric pressure
//' 
//' @description Calculate the atmospheric pressure using 
//' the altitude.
//' 
//' @param z a double representing the altitude \eqn{[masl]}.
//' 
//' @return A double with the atmospheric pressure \eqn{[kPa]}.
//' 

// [[Rcpp::export]]
double p_atm(double z) {
  return 101.3 * exp( -0.00013 * z ); // kPa
}