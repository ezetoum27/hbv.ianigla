#include <Rcpp.h>
#include "soil_hbv_pro.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_soil
//'
//' @title Soil moisture routine
//'
//' @description Allows you to account for actual
//' evapotranspiration, abstractions, antecedent conditions
//' and effective runoff. The formulation enables non
//' linear relationships between soil box water input
//' (rainfall plus snowmelt) and the effective runoff.
//'
//' @usage do_soil(
//'        m_qflow,
//'        m_pet,
//'        m_swe,
//'        swc,
//'        m_param
//'        )
//'
//' @param m_qflow numeric matrix with total water generated
//' in \code{\link{do_snow}} \eqn{[mm/\Delta t]}
//' (\code{ncol = number of zones}).
//'
//' @param m_pet numeric matrix with potential evapotranspiration
//' \eqn{[mm/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param m_swe numeric matrix with snow water equivalent
//' simulations (from \code{\link{do_snow}}) \eqn{[mm/\Delta t]}
//' (\code{ncol = number of zones}).
//'
//' @param swc double with initial soil water content
//' \eqn{[mm]}.
//'
//' @param m_param numeric matrix with parameters
//' (\code{ncol = number of zones}):
//' \enumerate{
//'
//'  \item \code{FC}: fictitious soil field capacity \eqn{[mm]}.
//'
//'  \item \code{LP}: parameter to get actual ET \eqn{[-]}.
//'
//'  \item \eqn{\beta}: exponential value that allows for
//'  non-linear relations between soil box water input
//'  (\code{qflow = melt + rainfall}) and the effective
//'  runoff \eqn{[-]}.
//'}
//'
//' @return List object with the following numeric matrices
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{soil_moisture} \eqn{[mm/\Delta t]}.
//'  \item \code{actual_evap} \eqn{[mm/\Delta t]}.
//'  \item \code{recharge} \eqn{[mm/\Delta t]}.
//' }
//'
//' @export
//'
// [[Rcpp::export]]
List do_soil(NumericMatrix m_qflow,
             NumericMatrix m_pet,
             NumericMatrix m_swe,
             double swc,
             NumericMatrix m_param){
  // iteraciones = nzones = columnas matrices
  int n_it = m_qflow.ncol();

  // filas
  int n = m_qflow.nrow();

  // matrices de salida
  NumericMatrix m_soil_moisture(n, n_it);
  NumericMatrix m_evap_actual(n, n_it);
  NumericMatrix m_recharge(n, n_it);

  for (int i = 0; i < n_it; ++i){

    // elementos de entrada
    NumericMatrix m_input(n, 3);

    m_input(_, 0) = m_qflow(_, i);
    m_input(_, 1) = m_pet(_, i);
    m_input(_, 2) = m_swe(_, i);

    NumericVector v_init_cond =
      NumericVector::create(swc, 1);

    NumericVector v_param = m_param(_, i);


    // rutina de suelo
    NumericMatrix m_soil =
      soil_hbv_pro(m_input,
                   v_init_cond,
                   v_param);

    // asigno columnas a la matriz general
    m_recharge(_, i) = m_soil(_, 0);
    m_evap_actual(_, i) = m_soil(_, 1);
    m_soil_moisture(_, i) = m_soil(_, 2);


  }

  // salida
  List soil_out =
    List::create(Named("soil_moisture") = m_soil_moisture,
                 Named("actual_evap") = m_evap_actual,
                 Named("recharge") = m_recharge);

  return soil_out;

}
