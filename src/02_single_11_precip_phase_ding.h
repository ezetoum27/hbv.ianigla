#include <Rcpp.h>

#ifndef PRECIP_PHASE_DING
#define PRECIP_PHASE_DING

Rcpp::NumericVector precip_phase_ding(double tair, 
                                      double rh,
                                      double z);
#endif