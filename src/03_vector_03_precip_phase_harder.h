#include <Rcpp.h>

#ifndef V_PRECIP_PHASE_HARDER
#define V_PRECIP_PHASE_HARDER

Rcpp::NumericMatrix v_precip_phase_harder(Rcpp::NumericVector tair,
                                          Rcpp::NumericVector rh,
                                          double delta_x = 0.01,
                                          double error_thres = 0.01,
                                          Rcpp::String period = "1 day");
#endif
