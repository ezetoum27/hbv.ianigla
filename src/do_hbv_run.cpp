#include <Rcpp.h>
#include "do_evap_hargreaves.h"
#include "do_glacier_route.h"
#include "do_glacier.h"
#include "do_pphase_harder.h"
#include "do_pphase_ding.h"
#include "do_precip_gradient.h"
#include "do_rescale.h"
#include "do_rh_gradient.h"
#include "do_route.h"
#include "do_snow.h"
#include "do_soil.h"
#include "do_tair_gradient.h"
#include "do_transfer.h"
using namespace Rcpp;

//+++++++++++++++++++++++++++++++++++++++++++++++++++
// Author       : Ezequiel Toum
// Licence      : GPL V3
// Institution  : IANIGLA-CONICET
// e-mail       : etoum@mendoza-conicet.gob.ar
//+++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
//+++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_hbv_run
//'
//' @title Run HBV model
//'
//' @description \code{do_hbv_run} allows you to run a full
//' hydro-glaciological model at once. This function is
//' designed to facilitate the use of the package, internally
//' coupling all the modules that HBV.IANIGLA offers. The
//' user only has to take care of providing the
//' input data, initial conditions and parameters.
//'
//' @usage
//' do_hbv_run(
//'        data_hipso,
//'        data_meteo,
//'        data_shortwave,
//'        station_altitude,
//'        rh_option,
//'        pphase_model,
//'        pphase_period,
//'        tair_param,
//'        precip_param,
//'        snow_init,
//'        snow_param,
//'        glacier_init,
//'        glacier_param,
//'        glacier_route_init,
//'        glacier_route_param,
//'        soil_init,
//'        soil_param,
//'        route_model,
//'        route_init,
//'        route_param,
//'        transfer_param)
//'
//' @param data_hipso data frame, data frame extension
//' (e.g. a tibble), or a lazy data frame (e.g. from
//' dbplyr or dtplyr) with the following columns (respect the
//' column names):
//' \enumerate{
//'
//'  \item \code{elev_band}: numeric column with the number of the
//'  elevation zone.
//'
//'  \item \code{hmin}: numeric column with the lower elevation
//'  of the elevation band \eqn{[masl]}.
//'
//'  \item \code{hmean}: numeric column with the mean elevation
//'  of the elevation band \eqn{[masl]}.
//'
//'  \item \code{hmax}: numeric column with the highest elevation
//'  of the elevation band \eqn{[masl]}.
//'
//'  \item \code{total_area}: numeric column with the total area of the
//'  elevation band \eqn{[km^2]}.
//'
//'  \item \code{glacier_area}: numeric column with the area covered
//'  by ice in the elevation band \eqn{[km^2]}.
//'
//' }
//'
//' @param data_meteo data frame, data frame extension
//' (e.g. a tibble), or a lazy data frame (e.g. from
//' dbplyr or dtplyr) with the following columns (respect the
//' column names):
//'
//' \enumerate{
//'
//'  \item \code{date}: date column with the dates
//'  of register meteorological data.
//'
//'  \item \code{tmin}: numeric vector with daily minimum
//'  air temperature \eqn{[°C]}.
//'
//'  \item \code{tmean}: numeric vector with daily mean
//'  air temperature \eqn{[°C]}.
//'
//'  \item \code{tmax}: numeric vector with daily maximum
//'  air temperature \eqn{[°C]}.
//'
//'  \item \code{rh}: numeric vector with daily mean
//'  relative humidity \eqn{[\%]}.
//'
//'  \item \code{precip}: numeric vector with daily total
//'  precipitation \eqn{[mm]}.
//'
//' }
//'
//' @param data_shortwave data frame, data frame extension
//' (e.g. a tibble), or a lazy data frame (e.g. from
//' dbplyr or dtplyr) with the following columns (respect the
//' column names):
//'
//' \enumerate{
//'
//'  \item \code{date}: date column with the dates. They should
//'  be the same of \code{data_meteo}
//'
//'  \item \code{kin_i}: daily total incoming extraterrestrial
//'  solar radiation \eqn{[MJ/(m^2 day)]}. The number of columns
//'  should be the same as number of elevation bands
//'  (\code{ncol = number of zones}).
//'
//'  }
//'
//' @param station_altitude numeric vector with two elements:
//'
//'  \enumerate{
//'
//'    \item air temperature and relative humidity station
//'    altitude \eqn{[masl]}.
//'
//'    \item precipitation station altitude \eqn{[masl]}.
//'
//'  }
//'
//'  The values are used to extrapolate these variables to other
//'  altitudes in the basin.
//'
//' @param rh_option string with assumption used to
//'  extrapolate relative humidity to other altitudes:
//'
//'  \enumerate{
//'
//'   \item \code{"e_vap"}: assumes that vapour pressure is
//'   constant over all altitudes. This
//'   value will be constrain to \code{rh = 100 \%}.
//'
//'   \item \code{"rh"}: assumes constant relative humidity over
//'   all altitudes.
//'
//'    }
//'
//' @param pphase_model string with model's choice: "harder" - "ding".
//'  Default is set to \code{"harder"}. See the works of
//'  Harder and Pomeroy (2013) or Ding et al. (2014) for more details.
//'
//' @param pphase_period  string with one of the following options
//' (just for \code{pphase_model = "harder"}):
//'
//' \enumerate{
//'  \item "15 min"
//'  \item "1 hour"
//'  \item "1 day"
//' }
//'
//' @param tair_param numeric vector with the following
//'  parameters:
//'
//'  \enumerate{
//'
//'   \item air temperature linear gradient [ºC/km].
//'   E.g.: \eqn{-6.5}.
//'
//'   \item threshold height. Air temperature does not decrease
//'   when the altitude is higher than this value \eqn{[masl]}.
//' }
//'
//' @param precip_param numeric vector with the following
//'  parameters:
//' \enumerate{
//'
//'   \item precipitation linear gradient increment  [\%/100 m].
//'
//'   \item threshold height. Precipitation does not decrease
//'   when the altitude is higher than this value \eqn{[masl]}.
//' }
//'
//' @param snow_init double with initial snow water equivalent
//'  \eqn{[mm]}. See also \code{\link{SnowGlacier_HBV}}.
//'
//' @param snow_param numeric matrix with the following
//'  parameters (\code{ncol = number of zones} -
//'  see also \code{\link{SnowGlacier_HBV}}):
//'
//' \enumerate{
//'   \item \code{SFCF}: snowfall correction factor \eqn{[-]}.
//'
//'   \item \code{Tt}: melt temperature \eqn{[ºC]}.
//'
//'   \item \code{fm}: snowmelt factor \eqn{[mm/°C.\Delta t]}.
//' }
//'
//' @param glacier_init numeric vector with the following
//'  initial values (see also \code{\link{SnowGlacier_HBV}}),
//'
//'  \enumerate{
//'
//'    \item double with initial snow water equivalent over
//'    glaciers \eqn{[mm we]}.
//'
//'    \item double with initial glacier volume over the entire
//'    basin \eqn{[mm we]}.
//'
//'  }
//'
//' @param glacier_param numeric matrix with the following
//'  parameters (\code{ncol = number of zones}):
//'
//' \enumerate{
//'
//'  \item \code{fi}: icemelt factor \eqn{[mm/°C.\Delta t]}.
//'
//'  \item \code{dt}: air temperature decrements \eqn{[ºC]}.
//'  This value will be subtracted from the air temperature
//'  series to take into account over-glacier reduced
//'  temperatures (regarding it's surroundings).
//' }
//'
//'  \code{SFCF}, \code{Tt} and \code{fm} are recycled from
//'  \code{snow_param}. See also \code{\link{SnowGlacier_HBV}}.
//'
//'
//' @param glacier_route_init numeric value with the initial
//'   glacier reservoir water content \strong{\code{SG}}
//'   \eqn{[mm]}. See also \code{\link{Glacier_Disch}}.
//'
//' @param glacier_route_param numeric vector with the
//'   following parameters (see \code{\link{Glacier_Disch}}):
//'
//'   \enumerate{
//'
//'    \item \code{KGmin}: minimal outflow rate
//'    \eqn{[1/\Delta t]}.
//'
//'    \item \code{dKG}:  maximum outflow rate
//'    increase \eqn{[1/\Delta t]}.
//'
//'    \item \code{AG}: scale factor \eqn{[mm]}.
//'  }
//'
//' @param soil_init double with initial soil water content
//'   \eqn{[mm]}.
//'
//' @param soil_param numeric matrix with the following
//'   parameters (\code{ncol = number of zones} - see
//'   \code{\link{Soil_HBV}} for more details):
//'
//'   \enumerate{
//'
//'    \item \code{FC}: fictitious soil field capacity \eqn{[mm]}.
//'
//'    \item \code{LP}: parameter to get actual ET \eqn{[-]}.
//'
//'    \item \eqn{\beta}: exponential value that allows for
//'    non-linear relations between soil box water input
//'    (\code{qflow = melt + rainfall}) and the effective
//'    runoff \eqn{[-]}.
//'
//'    }
//'
//' @param route_model numeric integer indicating which
//'   reservoir formulation to use (see \code{\link{Routing_HBV}}).
//'
//' @param route_init numeric vector with initial state variables
//'   (see \code{\link{Routing_HBV}}'s \code{initCond} argument).
//'
//' @param route_param numeric vector with model parameters
//'   (see \code{\link{Routing_HBV}}'s \code{param} argument).
//'
//' @param transfer_param numeric vector with the base transfer
//'   function (see \code{\link{UH}}).
//'
//' @return A list with the following elements:
//'
//' @references
//'
//'  \enumerate{
//'
//'    \item Harder, P., Pomeroy, J.W., Westbrook, C.J., 2015.
//'    Hydrological resilience of a Canadian Rockies headwaters
//'    basin subject to changing climate, extreme weather, and
//'    forest management. Hydrological Processes 29, 3905–3924.
//'    https://doi.org/10.1002/hyp.10596
//'
//'    \item Ding, B., Yang, K., Qin, J., Wang, L., Chen,
//'    Y., He, X., 2014. The dependence of precipitation types
//'    on surface elevation and meteorological conditions and
//'    its parameterization. Journal of Hydrology 513, 154–163.
//'    https://doi.org/10.1016/j.jhydrol.2014.03.038
//'
//'
//'  }
//'
//' @export
//'
// [[Rcpp::export]]
List do_hbv_run(DataFrame data_hipso,
                DataFrame data_meteo,
                DataFrame data_shortwave,
                NumericVector station_altitude,
                String rh_option,
                String pphase_model,
                String pphase_period,
                NumericVector tair_param,
                NumericVector precip_param,
                double snow_init,
                NumericMatrix snow_param,
                NumericVector glacier_init,
                NumericMatrix glacier_param,
                double glacier_route_init,
                NumericVector glacier_route_param,
                double soil_init,
                NumericMatrix soil_param,
                int route_model,
                NumericVector route_init,
                NumericVector route_param,
                NumericVector transfer_param){


  // +++++++++++++++++++
  // condicionales
  // +++++++++++++++++++


  // +++++++++++++++++++
  // vectores
  // +++++++++++++++++++

  NumericVector v_total_area   = data_hipso["total_area"];
  NumericVector v_glacier_area = data_hipso["glacier_area"];

  double basin_area   = sum( v_total_area );
  double glacier_area = sum( v_glacier_area );

  // calculo áreas relativas
  NumericVector v_rel_basin   = v_total_area / basin_area;
  NumericVector v_rel_glacier = v_glacier_area / basin_area;
  NumericVector v_rel_soil    = v_rel_basin - v_rel_glacier;

  // hipsometría glaciar
  NumericVector v_hipso_glacier = v_glacier_area / glacier_area;
  NumericVector v_mask_glacier  = ceil(v_hipso_glacier) * glacier_init(1);


  // +++++++++++++++++++
  // matrices
  // +++++++++++++++++++

  NumericMatrix m_glacier_param(5, v_total_area.size());

  m_glacier_param(0, _) = snow_param(0, _);
  m_glacier_param(1, _) = snow_param(1, _);
  m_glacier_param(2, _) = snow_param(2, _);

  m_glacier_param(3, _) = glacier_param(0, _);
  m_glacier_param(4, _) = glacier_param(1, _);

  int n_row = data_shortwave.nrow();
  int n_col = (data_shortwave.size() - 1 );

  NumericMatrix m_shortwave(n_row, n_col);

  for(int i = 0; i < n_col; i++){

    NumericVector v_aux = data_shortwave[i + 1];

    m_shortwave(_, i ) = v_aux;

  }

  // +++++++++++++++++++
  // meteo
  // +++++++++++++++++++

 // tair
  List hbv_tmin =
    do_tair_gradient(data_meteo["tmin"],
                     station_altitude(0),
                     data_hipso["hmean"],
                     tair_param);

  List hbv_tmean =
    do_tair_gradient(data_meteo["tmean"],
                     station_altitude(0),
                     data_hipso["hmean"],
                     tair_param);

  List hbv_tmax =
    do_tair_gradient(data_meteo["tmax"],
                     station_altitude(0),
                     data_hipso["hmean"],
                     tair_param);

  // rh
  List hbv_rh =
    do_rh_gradient(data_meteo["tmean"],
                   data_meteo["rh"],
                   station_altitude(0),
                   data_hipso["hmean"],
                   tair_param,
                   rh_option);

  // precip gradient
  List hbv_precip =
    do_precip_gradient(data_meteo["precip"],
                       station_altitude(1),
                       data_hipso["hmean"],
                       precip_param);

  List hbv_pphase;
  // precip phase
  if(pphase_model == "harder"){

    hbv_pphase =
      do_pphase_harder(hbv_tmean["tair"],
                       hbv_rh["rh"],
                       hbv_precip["precipitation"],
                       pphase_period);


  } else if(pphase_model == "ding"){

    hbv_pphase =
      do_pphase_ding(hbv_tmean["tair"],
                     hbv_rh["rh"],
                     hbv_precip["precipitation"],
                     data_hipso["hmean"]);

  } else{

    stop( "pphase_model not available!" );

  }


  // +++++++++++++++++++
  // evap
  // +++++++++++++++++++

  List hbv_ref_evap =
    do_evap_hargreaves(
      hbv_tmean["tair"],
      hbv_tmax["tair"],
      hbv_tmin["tair"],
      m_shortwave);


  // +++++++++++++++++++
  // criosfera
  // +++++++++++++++++++

  List hbv_snow =
    do_snow(hbv_tmean["tair"],
            hbv_pphase["rainfall"],
            hbv_pphase["snowfall"],
            snow_init,
            snow_param);

  List hbv_glacier =
      do_glacier(
        hbv_tmean["tair"],
        hbv_pphase["rainfall"],
        hbv_pphase["snowfall"],
        v_mask_glacier,
        glacier_init(0),
        m_glacier_param);

  // +++++++++++++++++++
  // suelo
  // +++++++++++++++++++

  List hbv_soil =
    do_soil(hbv_snow["qflow"],
            hbv_ref_evap["evap_ref"],
            hbv_snow["swe"],
            soil_init,
            soil_param);

  // +++++++++++++++++++
  // rescalo
  // +++++++++++++++++++

  NumericMatrix rescale_tmean =
    do_rescale(hbv_tmean,
               v_rel_basin);

  NumericMatrix rescale_rh =
      do_rescale(hbv_rh,
                 v_rel_basin);

  NumericMatrix rescale_pphase =
      do_rescale(hbv_pphase,
                 v_rel_basin);

  NumericMatrix rescale_snow =
      do_rescale(hbv_snow,
                 v_rel_soil);

 // a escala glaciar y suelo
 NumericMatrix rescale_glacier =
    do_rescale(hbv_glacier,
               v_hipso_glacier);

 NumericMatrix rescale_soil =
      do_rescale(hbv_soil,
                 v_rel_soil);

 // +++++++++++++++++++
 // reservorios
 // +++++++++++++++++++

  //  sale a escala de cuenca
 NumericVector v_swe   = rescale_glacier(_, 2);
 NumericVector v_water = rescale_glacier(_, 8);

 NumericMatrix m_rescale_gl (n_row, 2);

 m_rescale_gl(_, 0) = v_swe;
 m_rescale_gl(_, 1) = v_water;



 NumericMatrix hbv_route_glacier =
   do_glacier_route(
     1,
     m_rescale_gl,
     glacier_route_init,
     glacier_route_param ) *
       (sum(v_rel_glacier));

 NumericVector v_rech = rescale_soil(_, 2);

 NumericMatrix hbv_route =
     do_route(route_model,
              v_rech,
              route_init,
              route_param);

 // +++++++++++++++++++
 // transferencia
 // +++++++++++++++++++

 NumericVector v_route_soil    = hbv_route(_, 0);
 NumericVector v_route_glacier = hbv_route_glacier(_, 0);

 NumericVector hbv_transfer =
   do_transfer(1,
               v_route_soil,
               transfer_param);

 NumericVector hbv_transfer_gl =
     do_transfer(1,
                 v_route_glacier,
                 transfer_param);

 NumericVector hbv_transfer_basin =
     hbv_transfer + hbv_transfer_gl;

 NumericMatrix hbv_q_out =
   cbind( hbv_transfer_gl,
          hbv_transfer_basin);

 colnames(hbv_q_out) = CharacterVector::create("q_glacier", "q_total");


  // salida
  List hbv_out =
    List::create(Named("tmin")     = hbv_tmin,
                 Named("tmean")    = hbv_tmean,
                 Named("tmax")     = hbv_tmax,
                 Named("rh")       = hbv_rh,
                 Named("precip")   = hbv_pphase,
                 Named("evap_ref") = hbv_ref_evap,
                 Named("snow")     = hbv_snow,
                 Named("glacier")  = hbv_glacier,
                 Named("soil")     = hbv_soil,
                 Named("basin_tmean")    = rescale_tmean,
                 Named("basin_rh")       = rescale_rh,
                 Named("basin_pphase")   = rescale_pphase,
                 Named("basin_snow")     = rescale_snow,
                 Named("basin_glacier")  = rescale_glacier,
                 Named("basin_soil")     = rescale_soil,
                 Named("basin_route")    = hbv_route,
                 Named("basin_route_gl") = hbv_route_glacier,
                 Named("basin_qflow")    = hbv_q_out);

  return hbv_out;

}


