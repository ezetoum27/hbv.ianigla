#include <Rcpp.h>
#ifndef UH_HBV
#define UH_HBV

Rcpp::NumericVector UH(int model,
                       Rcpp::NumericVector Qg,
                       Rcpp::NumericVector param);
#endif
