#include <Rcpp.h>
using namespace Rcpp;

//' @name dream_rowbind
//'
//' @title rowbind
//'
//' @description Binds a numeric vector \code{v}
//' as the new last row of the numeric matrix \code{m}.
//'
//' @param m numeric matrix
//'
//' @param v numeric vector
//'
//' @return Numeric matrix


// [[Rcpp::export]]
NumericMatrix dream_rowbind(NumericMatrix m,
                            NumericVector v){
  int n_col = m.ncol();
  int n_row = m.nrow();

  NumericMatrix m_out(n_row + 1, n_col);

  for(int i = 0; i < n_col; i++){
      m_out(_, i) = m(_, i);
    }

  m_out(n_row, _) = v;

  return m_out;


  }


// NumericVector dream_vector(NumericMatrix m,
//                            NumericVector v){
//   int n_col = m.ncol();
//   int n_row = m.nrow();
//   int n_dim = n_col * n_row;
//
//   NumericMatrix t_m = transpose(m);
//   t_m.attr("dim") = Dimension(n_dim, 1);
//
//   NumericVector v_out = as<NumericVector>(t_m);
//
//   for(int i = 0; i < n_col; i++){
//     v_out.push_back(v(i));
//   }
//
//   v_out.attr("dim") = Dimension(n_col, n_row + 1);
//   NumericMatrix m_out = as<NumericMatrix>(v_out);
//
//   return transpose(m_out);
//
//
// }
