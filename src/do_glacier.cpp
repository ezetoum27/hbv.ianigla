#include <Rcpp.h>
#include "aa_icemelt.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_glacier
//'
//' @title Glacier melt routine
//'
//' @description Allows you to simulate snow accumulation,
//' snow and ice melting processes using a temperature
//' index approach.
//'
//' @usage do_glacier(
//'        m_tair,
//'        m_rainfall,
//'        m_snowfall,
//'        v_glacier_vol,
//'        swe_init,
//'        m_param
//'        )
//'
//' @param m_tair numeric matrix with air temperatures
//' \eqn{[°C/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param m_rainfall numeric matrix with rainfall
//' \eqn{[mm/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param m_snowfall numeric matrix with snowfall
//' \eqn{[mm/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param v_glacier_vol numeric vector with glacier
//' volume \eqn{[mm \, we]} (\code{length = number of zones}).
//'
//' @param swe_init double with initial snow water equivalent
//' over the glacier \eqn{[mm]}.
//'
//' @param m_param numeric matrix with parameters
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{SFCF}: snowfall correction factor \eqn{[-]}.
//'  \item \code{Tt}: melt temperature \eqn{[ºC]}.
//'  \item \code{fm}: snowmelt factor \eqn{[mm/°C.\Delta t]}.
//'  \item \code{fi}: icemelt factor \eqn{[mm/°C.\Delta t]}.
//'  \item \code{dt}: air temperature decrements \eqn{[ºC]}.
//'  This value will be subtracted from the air temperature
//'  series to take into account over-glacier reduced
//'  temperatures (regarding it's surroundings).
//'}
//'
//' @return List object with the following numeric matrices
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{rainfall} \eqn{[mm/\Delta t]}.
//'  \item \code{snowfall} \eqn{[mm/\Delta t]}.
//'  \item \code{swe} \eqn{[mm/\Delta t]}.
//'  \item \code{snow_melt} \eqn{[mm/\Delta t]}.
//'  \item \code{ice_melt} \eqn{[mm/\Delta t]}.
//'  \item \code{total_melt = snow_melt + ice_melt}
//'  \eqn{[mm/\Delta t]}.
//'  \item \code{ice_vol} \eqn{[mm]}.
//'  \item \code{cum = snowfall - total_melt} \eqn{[mm/\Delta t]}.
//'  \item \code{water = rainfall + total_melt}
//'  \eqn{[mm/\Delta t]}.
//' }
//'
//' @export
//'
// [[Rcpp::export]]
List do_glacier(NumericMatrix m_tair,
                NumericMatrix m_rainfall,
                NumericMatrix m_snowfall,
                NumericVector v_glacier_vol,
                double swe_init,
                NumericMatrix m_param){

  // iteraciones = nzones = columnas matrices
  int n_it = m_tair.ncol();

  // filas
  int n = m_tair.nrow();

  // matrices de salida
  NumericMatrix m_rain(n, n_it);
  NumericMatrix m_snow(n, n_it);
  NumericMatrix m_swe(n, n_it);

  NumericMatrix m_snow_melt(n, n_it);
  NumericMatrix m_ice_melt(n, n_it);
  NumericMatrix m_total_melt(n, n_it);

  NumericMatrix m_ice_vol(n, n_it);
  NumericMatrix m_cum(n, n_it);
  NumericMatrix m_water(n, n_it);

  // itero sobre las zonas
  for (int i = 0; i < n_it; ++i){

    // elementos de entrada
    NumericMatrix m_input(n, 3);
    NumericVector v_tair = m_tair(_, i);
    NumericVector v_rain = m_rainfall(_, i);
    NumericVector v_snow = m_snowfall(_, i);

    NumericVector v_init_cond =
      NumericVector::create( swe_init, v_glacier_vol[i] );

    NumericVector v_param = m_param(_, i);


    // asigno
    m_input(_, 0) = v_tair;
    m_input(_, 1) = v_rain;
    m_input(_, 2) = v_snow;


    // rutina glaciar
    NumericMatrix m_glacier_model =
      icemelt_pro(m_input,
                  v_init_cond,
                  v_param);

    // asigno columnas a la matriz general
    m_rain(_, i)  = m_glacier_model(_, 0);
    m_snow(_, i)  = m_glacier_model(_, 1);
    m_swe(_, i)   = m_glacier_model(_, 2);

    m_snow_melt(_, i)  = m_glacier_model(_, 3);
    m_ice_melt(_, i)   = m_glacier_model(_, 4);
    m_total_melt(_, i) = m_glacier_model(_, 5);

    m_ice_vol(_, i) = m_glacier_model(_, 6);
    m_cum(_, i)     = m_glacier_model(_, 7);
    m_water(_, i)   = m_glacier_model(_, 8);



  }

  // salida
  List glacier_out =
    List::create(Named("rainfall")   = m_rain,
                 Named("snowfall")   = m_snow,
                 Named("swe")        = m_swe,
                 Named("snow_melt")  = m_snow_melt,
                 Named("ice_melt")   = m_ice_melt,
                 Named("total_melt") = m_total_melt,
                 Named("ice_vol")    = m_ice_vol,
                 Named("cum")        = m_cum,
                 Named("water")      = m_water);


  return glacier_out;

}
