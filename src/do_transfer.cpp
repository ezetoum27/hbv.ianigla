#include <Rcpp.h>
#include "UH_HBV.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_transfer
//'
//' @title Transfer function
//'
//' @description Use a triangular transfer function
//' to adjust the timing of the simulated streamflow
//' discharge. This module represents the runoff routing
//' in the streams.
//'
//' @usage do_transfer(
//'        model,
//'        v_qflow,
//'        v_param
//'        )
//'
//' @param model numeric integer with the transfer
//' function model (see \code{\link{UH}}).
//'
//' @param v_qflow numeric vector with the water
//' that gets into the stream (see \code{\link{UH}}).
//'
//' @param v_param numeric vector with the base transfer
//' function (see \code{\link{UH}}).
//'
//' @return Numeric vector with the simulated
//' streamflow discharge.
//'
//' @export
//'
// [[Rcpp::export]]
NumericVector do_transfer(int model,
                          NumericVector v_qflow,
                          NumericVector v_param){

  NumericVector transfer = UH(model, v_qflow, v_param);

  return transfer;


}
