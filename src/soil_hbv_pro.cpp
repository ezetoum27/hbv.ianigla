#include <Rcpp.h>
using namespace Rcpp;

// **********************************************************
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
//  **********************************************************
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
//  **********************************************************

//' @name soil_hbv_pro
//'
//' @title Empirical soil moisture routine
//'
//' @description Accounts for actual evapotranspiration,
//' abstractions, antecedent conditions and effective runoff.
//' The formulation enables non linear relationships between
//' soil box water input (rainfall plus snowmelt) and the
//' effective runoff. This effective value is the input series
//' to the routine function (\code{\link{do_route}}).
//'
//' @usage soil_hbv_pro(
//'        inputData,
//'        initCond,
//'        param
//'        )
//'
//' @param inputData numeric matrix with the following series:
//'
//' \itemize{
//'
//'   \item \code{column_1}: \code{Total = Prain + Msnow} \eqn{[mm/\Delta t]}.
//'   This series comes from the output of the \code{\link{do_snow}} module.
//'
//'   \item \code{column_2}: potential evapotranspiration  \eqn{[mm/\Delta t]}.
//'
//'   \item \code{column_3}: snow water equivalent \eqn{[mm/\Delta t]}. If snow
//'   exists above the ground, actual evap. will be zero.
//' }
//'
//'
//' @param initCond numeric vector with the following values:
//'  \enumerate{
//'
//'   \item initial soil water content \eqn{[mm]}. This is a model state
//'   variable and is internally used as first soil moisture value.
//'
//'   \item relative area \eqn{[-]}. Default value is one.
//'}
//'
//' @param param numeric vector with the following values:
//' \enumerate{
//'
//'   \item \code{FC}: fictitious soil field capacity \eqn{[mm]}.
//'
//'   \item \code{LP}: parameter to get actual ET \eqn{[-]}.
//'
//'   \item \eqn{\beta}: exponential value that allows for non-linear
//'   relations between soil box water input (rainfall plus snowmelt)
//'   and the effective runoff \eqn{[-]}.
//' }
//'
//' @return Numeric matrix with the following columns:
//' \enumerate{
//'
//'   \item \code{Rech}: recharge series \eqn{[mm/\Delta t]}.
//'
//'   \item \code{Eact}: actual evapotranspiration series \eqn{[mm/\Delta t]}.
//'
//'   \item \code{SM}: soil moisture series \eqn{[mm/\Delta t]}.
//' }


 // [[Rcpp::export]]
 NumericMatrix soil_hbv_pro(NumericMatrix inputData,
                            NumericVector initCond,
                            NumericVector param) {
   // *********************
   //  conditionals
   // *********************

   // check for NA_real_
   // inputData
   int chk_1 = sum( is_na(inputData) );
   if(chk_1 != 0){

     stop("inputData argument should not contain NA values!");

   }

   // initCond
   int chk_2 = sum( is_na(initCond) );
   if(chk_2 != 0){

     stop("initCond argument should not contain NA values!");

   }

   // param
   int chk_3 = sum( is_na(param) );
   if(chk_3 != 0){

     stop("param argument should not contain NA values!");

   }

   // *********************
   //  models
   // *********************

   int n = inputData.nrow(); // número de filas
   int m = 3; //número de columnas

   // MODELO CLÁSICO

     // verifico condiciones
     if (inputData.ncol() < 2) {
       stop("Please verify the inputData matrix");
     }
     if (initCond.size() < 2) {
       stop("Please verify the initCond vector");
     }
     if (param.size() < 3) {
       stop("Please verify the param vector");
     }
     if (param[0] <= 0) {
       stop("Verify: FC > 0");
     }
     if ( (param[1] > 1) || (param[1] <= 0) ) {
       stop("Verify: 0 < LP <= 1");
     }

     // defino variables, parámetros y matriz de salida
     double Eac, Ieff, Def, SM; // variables intermedias y finales
     double FC, LP, beta;       // parámetros
     NumericMatrix out(n, m);   // matriz de salida

     // Le doy valores a los parámetros
     FC   = param[0];
     LP   = param[1];
     beta = param[2];

     for (int i = 0; i < n; ++i) {
       if (i == 0) { // primera corrida

         // me aseguro que el usuario no cometa el error de colocar SM incial > FC
         if (initCond[0] <= param[0]){
           SM = initCond[0];
         } else {
           SM = param[0];
         }

         if( inputData(i, 2) > 1 ){

           Eac = 0.0;

         } else{

           Eac  = inputData(i, 1) * std::min(SM / (FC * LP), 1.0);

         }



         Ieff = inputData(i, 0) * std::pow( (SM / FC), beta );
         Def  = SM + inputData(i, 0) - Ieff - Eac;

         // me aseguro que cierre el balance de masa
         if (Def < 0.0) {
           Eac = SM;
           SM  = 0.0;

         } else if (Def <= FC){
           SM  = Def;

         } else {//caso en que la humedad del suelo es mayor a FC
           Ieff = Ieff + (Def - FC);
           SM   = FC;
         }

       } else {

         if( inputData(i, 2) > 1 ){

           Eac = 0.0;

         } else{

           Eac  = inputData(i, 1) * std::min(SM / (FC * LP), 1.0);

         }

         // Eac  = inputData(i, 1) * std::min(SM / (FC * LP), 1.0);
         Ieff = inputData(i, 0) * std::pow( (SM / FC), beta );
         Def  = SM + inputData(i, 0) - Ieff - Eac;

         // me aseguro que cierre el balance de masa
         if (Def < 0.0) {
           Eac = SM;
           SM  = 0.0;

         } else if (Def <= FC){
           SM  = Def;

         } else { //caso en que la humedad del suelo es mayor a FC
           Ieff = Ieff + (Def - FC);
           SM   = FC;
         }
       }

       // Armo matriz de salida
       out(i, 0) = Ieff * initCond[1]; //Recarga
       out(i, 1) = Eac;
       out(i, 2) = SM;

     }

     colnames(out) = CharacterVector::create("Rech", "Eac", "SM");
     return out;




 }
