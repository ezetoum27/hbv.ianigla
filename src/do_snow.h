#include <Rcpp.h>
#ifndef DO_SNOW
#define DO_SNOW

Rcpp::List do_snow(Rcpp::NumericMatrix m_tair,
                   Rcpp::NumericMatrix m_rainfall,
                   Rcpp::NumericMatrix m_snowfall,
                   double swe_init,
                   Rcpp::NumericMatrix m_param);
#endif
