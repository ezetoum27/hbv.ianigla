#include <Rcpp.h>
#include "03_vector_03_precip_phase_harder.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_pphase_harder
//'
//' @title Precipitation phase partitioning
//'
//' @description Allows you to estimate precipitation phase
//' using a psychrometric energy balance method (Harder and Pomeroy, 2013).
//'
//' @usage do_pphase_harder(
//'        m_tair,
//'        m_rh,
//'        m_precip,
//'        period = "1 day"
//'        )
//'
//' @param m_tair numeric matrix with air temperatures
//' \eqn{[°C/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param m_rh numeric matrix with relative
//' humidity \eqn{[\%]} (\code{ncol = number of zones}).
//'
//' @param m_precip numeric matrix with total precipitation
//' \eqn{[mm/\Delta t]} (\code{ncol = number of zones}).
//'
//' @param period string with the recorded precipitation
//' time-step:
//' \enumerate{
//'  \item \code{"1 day"} (default).
//'  \item \code{"1 hour"}.
//'  \item \code{"15 min"}.
//' }
//'
//' @return List object with the following numeric matrices
//' (\code{ncol = number of zones}):
//' \enumerate{
//'  \item \code{rainfall} \eqn{[mm/\Delta t]}.
//'  \item \code{snowfall} \eqn{[mm/\Delta t]}.
//' }
//'
//' @export
//'
// [[Rcpp::export]]
 List do_pphase_harder(NumericMatrix m_tair,
                       NumericMatrix m_rh,
                       NumericMatrix m_precip,
                       String period = "1 day"){

   // iteraciones = nzones = columnas matrices
   int n_it = m_tair.ncol();

   // filas
   int n = m_tair.nrow();

   // matrices de salida
   NumericMatrix m_rainfall(n, n_it);
   NumericMatrix m_snowfall(n, n_it);

   // itero sobre las zonas
   for (int i = 0; i < n_it; ++i){

     // elementos de entrada
     NumericVector v_tair = m_tair(_, i);
     NumericVector v_rh   = m_rh(_, i);
     NumericVector v_prec = m_precip(_, i);

     // modelo harder
     NumericMatrix m_harder =
       v_precip_phase_harder(v_tair,
                             v_rh,
                             0.01,
                             0.01,
                             period);

     // asigno valores
     m_snowfall(_, i) = m_harder(_, 0) * v_prec;
     m_rainfall(_, i) = m_harder(_, 1) * v_prec;


   }

   // salida
   List harder_out =
     List::create(Named("rainfall") = m_rainfall,
                  Named("snowfall") = m_snowfall);

   return harder_out;

 }
