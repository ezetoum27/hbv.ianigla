#include <Rcpp.h>
#ifndef SNOWMELT
#define SNOWMELT

Rcpp::NumericMatrix snowmelt(Rcpp::NumericMatrix inputData,
                             Rcpp::NumericVector initCond,
                             Rcpp::NumericVector param);

#endif

#ifndef SNOWMELT_PRO
#define SNOWMELT_PRO

Rcpp::NumericMatrix snowmelt_pro(Rcpp::NumericMatrix inputData,
                                 Rcpp::NumericVector initCond,
                                 Rcpp::NumericVector param);

#endif
