#include <Rcpp.h>
#ifndef DO_RH_GRADIENT
#define DO_RH_GRADIENT

Rcpp::List do_rh_gradient(Rcpp::NumericVector v_tair,
                          Rcpp::NumericVector v_rh,
                          double station_alt,
                          Rcpp::NumericVector v_altitude,
                          Rcpp::NumericVector v_grad,
                          Rcpp::String option = "e_vap");
#endif
