#include <Rcpp.h>
#include "Routing_HBV.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_route
//'
//' @title Routing bucket function
//'
//' @description Implement one of the five different
//' bucket formulations for runoff routing. The output
//' of this function is the input series of the
//' transfer function (\code{\link{do_transfer}}).
//'
//' @usage do_route(
//'        model,
//'        v_recharge,
//'        v_init_cond,
//'        v_param
//'        )
//'
//' @param model numeric integer indicating which
//' reservoir formulation to use (see \code{\link{Routing_HBV}}).
//'
//' @param v_recharge numeric vector with recharge from soil routine
//' (after applying \code{do_rescale}).
//'
//' @param v_init_cond numeric vector with initial state variables
//' (see \code{\link{Routing_HBV}}'s \code{initCond} argument).
//'
//' @param v_param numeric vector with model parameters
//' (see \code{\link{Routing_HBV}}'s \code{param} argument).
//'
//' @return Numeric matrix with the columns depending upon
//' \code{model} choice (see \code{\link{Routing_HBV}}).
//'
//' @export
//'
// [[Rcpp::export]]
NumericMatrix do_route(int model,
                       NumericVector v_recharge,
                       NumericVector v_init_cond,
                       NumericVector v_param){

  NumericMatrix m_recharge(v_recharge.length(), 1);

  m_recharge(_, 0) = v_recharge;

  NumericMatrix route_output =
    Routing_HBV(model,
                false,
                m_recharge,
                v_init_cond,
                v_param);

  return route_output;



}
