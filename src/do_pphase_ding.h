#include <Rcpp.h>
#ifndef DO_PPHASE_DING
#define DO_PPHASE_DING

Rcpp::List do_pphase_ding(Rcpp::NumericMatrix m_tair,
                          Rcpp::NumericMatrix m_rh,
                          Rcpp::NumericMatrix m_precip,
                          Rcpp::NumericVector v_altitude);
#endif
