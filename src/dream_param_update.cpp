#include <Rcpp.h>
#include "dream_dmultinom.h"
using namespace Rcpp;

//' @name dream_param_update
//'
//' @title Update parameters
//'
//' @description Decides which parameter to update
//'
//' @param n_param integer with parameter vector size.
//'
//' @param CR integer with crossover values. It defines the
//' size of the probability vector.
//'
//' @return Integer binary vector. A \code{1} value indicates
//' parameter update.


// [[Rcpp::export]]
IntegerVector dream_param_update(int n_param,
                                 int CR = 3
                                 ){
  // Algorithm 4 Subspace sampling - Sadegh and Vrugt (2014)
  // note: iteration over K chains is made in the general
  // do_dream() function.

  // define crossover odds

  NumericVector odds(CR);
  for(int i = 0; i < CR; i++){
    odds(i) = runif(1, 0, 1)[0]; // my modification to randomize
    // odds(i) = (i + 1) / CR;
  }

  odds = odds / sum(odds);

  //  Define A to be an empty set
  IntegerVector A(n_param);

  // Sample P from the discrete multinomial distribution
  unsigned int n = 1;
  unsigned int size = 1;

  IntegerMatrix P = rmultinom_rcpp(n, size, odds);

  int z = which_max( P(_, 0) ); // position

  double p = 1 - odds(z); // odd value

  // Draw d labels from a multivariate uniform distribution
  NumericVector u_vect = runif(n_param, 0, 1);

  // iterate through vector
  int flag = 0; // no single parameter

  for(int j = 0; j < n_param; j++){

    if( u_vect(j) > p){
      A(j) = 1;
      flag = 1;

    } else{
      A(j) = 0;
    }


  }

  if(flag == 0){

    int d_random = runif(1, 0, n_param - 1)[0];

    A(d_random) = 1;

  }

  return A;


}
