#include <Rcpp.h>
#ifndef ROUTING_HBV
#define ROUTING_HBV

Rcpp::NumericMatrix Routing_HBV(int model,
                                bool lake,
                                Rcpp::NumericMatrix inputData,
                                Rcpp::NumericVector initCond,
                                Rcpp::NumericVector param);
#endif
