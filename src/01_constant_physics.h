#include <Rcpp.h>

#ifndef R_DRY_AIR
#define R_DRY_AIR

double R_dry_air();
#endif


#ifndef LAMBDA_F
#define LAMBDA_F

double lambda_f();
#endif


#ifndef CP_WATER
#define CP_WATER

double cp_water();
#endif


#ifndef LAMBDA_V
#define LAMBDA_V

double lambda_v(double t_surface);
#endif


#ifndef M_WATER
#define M_WATER

double m_water();
#endif


#ifndef R_GAS
#define R_GAS

double R_gas();
#endif


#ifndef SIGMA_STEFAN
#define SIGMA_STEFAN

double sigma_stefan();
#endif


#ifndef PRECIP_WATER
#define PRECIP_WATER

double precip_water(double t_air,
                    double rh);
#endif