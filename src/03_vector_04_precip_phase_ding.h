#include <Rcpp.h>

#ifndef V_PRECIP_PHASE_DING
#define V_PRECIP_PHASE_DING

Rcpp::NumericMatrix v_precip_phase_ding(Rcpp::NumericVector tair,
                                        Rcpp::NumericVector rh,
                                        double z);
#endif
