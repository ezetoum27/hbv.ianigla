#include <Rcpp.h>
#include "02_single_01_patm.h"
#include "02_single_02_e_sat.h"
using namespace Rcpp;

// **********************************************************
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
//  **********************************************************
//  hydroflex package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
//  **********************************************************

// INTERNAL
// SOURCE: Ding y col. (2014)

//' @name precip_phase_ding
//'
//' @title Precipitation phase - Ding et al. (2014)
//' 
//' @description Estimate precipitation phase partioning
//' based on Tair and RH
//' 
//' @param tair recorded air temperature \eqn{[ºC]}.
//' @param rh relative humidity \eqn{[\%]}. 
//' @param z double with meteorological station altitude \eqn{[masl]}.
//' 
//' @note The model was developed using daily data. 
//'  
//' @return A numeric vector with:
//' * the snowfall fraction \eqn{[-]}
//' * the rainfall fraction \eqn{[-]}

// [[Rcpp::export]]
NumericVector precip_phase_ding(double tair, 
                                double rh,
                                double z){
  
  double twb, e_ast, Delta_tair, p_z, to, Delta_S;
  double f_snow, f_rain;
  
  //+++1
  e_ast = e_sat(tair); // [kPa]
  p_z   = p_atm(z); // [kPa]
  
  Delta_tair = ( 2508.3 / pow( (tair + 237.3), 2 ) ) * 
    exp( 17.3 * tair / (tair + 237.3) ); // [kPa / ºC]
  
  // temperatura de bulbo húmedo
  twb = tair - ( e_ast * (1.0 - (rh / 100) ) ) / 
    (Delta_tair + 0.000643 * p_z);//  [ºC]
  
  
  //+++2
  to = -5.87 - 1.042 / pow(10, 4) * z + 8.85 / pow(10, 8) * pow(z, 2) +
    16.06 * (rh / 100) - 9.614 * pow( (rh / 100), 2 );
  
  Delta_S = 2.374 - 1.634 * (rh / 100);
  
  //+++3
  f_snow = 1 / ( 1 + exp( (twb - to) /  Delta_S) );
  f_rain = 1 - f_snow;
  
  NumericVector precip_phase = 
    NumericVector::create(f_snow, f_rain);
  
  return round(precip_phase, 2);
  
  
}