#include <Rcpp.h>
#ifndef DO_ROUTE
#define DO_ROUTE

Rcpp::NumericMatrix do_route(int model,
                             Rcpp::NumericVector v_recharge,
                             Rcpp::NumericVector v_init_cond,
                             Rcpp::NumericVector v_param);
#endif
