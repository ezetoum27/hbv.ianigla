#include <Rcpp.h>
#ifndef DO_PRECIP_GRADIENT
#define DO_PRECIP_GRADIENT

Rcpp::List do_precip_gradient(Rcpp::NumericVector v_precip,
                              double station_alt,
                              Rcpp::NumericVector v_altitude,
                              Rcpp::NumericVector v_grad);
#endif
