#include <Rcpp.h>
#include "02_single_11_precip_phase_ding.h"
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++
//  hydroflex package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++

// INTERNAL
// SOURCE: Ding et al (2014)

//' @name v_precip_phase_ding
//'
//' @title Precipitation phase - Ding et al. (2014).
//' 
//' @description Estimate precipitation phase partioning
//' based on Tair and RH. Vectorised version.
//' 
//' @param tair rnumeric vecto with recorded air temperature \eqn{[ºC]}.
//' @param rh numeric vector with relative humidity \eqn{[\%]}. 
//' @param z double with meteorological station altitude \eqn{[masl]}.
//' 
//' @note The model was developed using daily data. 
//'  
//' @return A numeric vector with:
//' * the snowfall fraction \eqn{[-]}
//' * the rainfall fraction \eqn{[-]}

// [[Rcpp::export]]
NumericMatrix v_precip_phase_ding(NumericVector tair, 
                                  NumericVector rh,
                                  double z){
  int n_it = tair.length();
  NumericMatrix ding_out(n_it, 2);
  
  
  for(int i = 0; i < n_it; i++){
    NumericVector p_phase = 
      precip_phase_ding(tair(i),
                        rh(i),
                        z);
    
    ding_out(i, 0) = p_phase(0); // fsnow
    ding_out(i, 1) = p_phase(1); // frain
    
  }
  
  return ding_out;
}