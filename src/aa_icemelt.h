#include <Rcpp.h>
#ifndef ICEMELT
#define ICEMELT

Rcpp::NumericMatrix icemelt(Rcpp::NumericMatrix inputData,
                            Rcpp::NumericVector initCond,
                            Rcpp::NumericVector param);
#endif

#ifndef ICEMELT_PRO
#define ICEMELT_PRO

Rcpp::NumericMatrix icemelt_pro(Rcpp::NumericMatrix inputData,
                                Rcpp::NumericVector initCond,
                                Rcpp::NumericVector param);
#endif
