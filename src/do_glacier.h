#include <Rcpp.h>
#ifndef DO_GLACIER
#define DO_GLACIER

Rcpp::List do_glacier(Rcpp::NumericMatrix m_tair,
                      Rcpp::NumericMatrix m_rainfall,
                      Rcpp::NumericMatrix m_snowfall,
                      Rcpp::NumericVector v_glacier_vol,
                      double swe_init,
                      Rcpp::NumericMatrix m_param);
#endif
