#include <Rcpp.h>
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_precip_gradient
//'
//' @title Precipitation extrapolation model
//'
//' @description Computes precipitation using an extrapolation
//' linear model.
//'
//' @usage do_precip_gradient(
//'        v_precip,
//'        station_alt,
//'        v_altitude,
//'        v_grad
//'        )
//'
//' @param v_precip numeric vector with precipitation series
//' \eqn{[mm]}.
//'
//' @param station_alt altitude of the precipitation station
//' \eqn{[masl]}
//'
//' @param v_altitude numeric vector with target altitudes
//' \eqn{[masl]} (\code{length = number of zones}).
//'
//' @param v_grad numeric vector with the following elements:
//' \enumerate{
//'
//'   \item precipitation linear gradient increment  [\%/100 m].
//'
//'   \item threshold height. Precipitation does not decrease
//'   when the altitude is higher than this value \eqn{[masl]}.
//' }
//'
//' @return List with extrapolated precipitation numeric matrix
//' (\code{ncol = number of zones}).
//'
//' @export
//'
// [[Rcpp::export]]
List do_precip_gradient(NumericVector v_precip,
                        double station_alt,
                        NumericVector v_altitude,
                        NumericVector v_grad){

  int n_row = v_precip.size();
  int n_col = v_altitude.size();

  NumericMatrix m_out(n_row, n_col);

  double grad_precip = v_grad(0);
  double grad_thres  = v_grad(1); // altitudinal threshold

  for(int i = 0; i < n_col; ++i) {

  for(int j = 0; j < n_row; ++j){

  if( v_precip(j) <= 0.001 ){

  m_out(j, i) = 0.0 ;

  } else{

  if(v_altitude(i) < grad_thres){

   m_out(j, i) =
     std::max(  (1 + (v_altitude(i) - station_alt) *
                grad_precip / (100 * 100) ) * v_precip(j),
              0.0);


  } else {

   m_out(j, i) =
     std::max(  (1 + (grad_thres - station_alt) *
     grad_precip / (100 * 100) ) * v_precip(j),
     0.0);
  }



  }




  }




  }

  // salida
  List prec_grad_out =
    List::create(Named("precipitation") = m_out);

  return prec_grad_out;


}
