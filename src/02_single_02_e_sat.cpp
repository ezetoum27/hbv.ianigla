#include <Rcpp.h>
using namespace Rcpp;

// **********************************************************
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
//  **********************************************************
//  hydroflex package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
//  **********************************************************

// INTERNAL
// SOURCE: Dingman (2014)

//' @name e_sat
//'
//' @title Saturated water vapor pressure
//' 
//' @description Calculates e*
//' 
//' @param tair a double with air temperature \eqn{[ºC]}.
//' 
//' @return A double with saturated water vapor pressure \eqn{[kPa]}.
//' 

// [[Rcpp::export]]
double e_sat(double tair) {
  
  double vap_sat;
  
  if(tair >= 0.00){
    
    vap_sat = 0.611 * exp( (17.27 * tair) / (tair + 237.3) );
      
  } else {
    
    vap_sat = 0.611 * exp( (21.87 * tair) / (tair + 265.5) );
  }
  
  
  return vap_sat;
}
