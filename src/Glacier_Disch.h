#include <Rcpp.h>
#ifndef GLACIER_DISCH
#define GLACIER_DISCH

Rcpp::NumericMatrix Glacier_Disch(int model,
                                  Rcpp::NumericMatrix inputData,
                                  double initCond,
                                  Rcpp::NumericVector param);
#endif
