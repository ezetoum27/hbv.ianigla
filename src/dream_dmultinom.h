#include <Rcpp.h>
#ifndef DREAM_DMULTINOM
#define DREAM_DMULTINOM

Rcpp::IntegerMatrix rmultinom_rcpp(unsigned int &n,
                                   unsigned int &size,
                                   Rcpp::NumericVector &probs);
#endif
