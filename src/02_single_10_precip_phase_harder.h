#include <Rcpp.h>

#ifndef PRECIP_PHASE_HARDER_SECANT
#define PRECIP_PHASE_HARDER_SECANT

Rcpp::NumericVector precip_phase_harder_secant(double tair,
                                               double rh,
                                               double delta_x,
                                               double error_thres, 
                                               int period);
#endif