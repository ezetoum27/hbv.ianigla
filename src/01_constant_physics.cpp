#include <Rcpp.h>
using namespace Rcpp;

// physical constants
// source: https://stackoverflow.com/questions/36777431/how-to-export-constants-from-c-into-r-with-rcpp

// [[Rcpp::export]]
double R_dry_air(){
  // constante de gas en condiciones secas
  return 287.00; // J / (kg * K)
}

// [[Rcpp::export]]
double lambda_f(){
  // calor latente de fusión del agua
  return 0.334; // MJ / kg
}

// [[Rcpp::export]]
double cp_water(){
  // capacidad calorífica del agua
  return 4.187 / 1000; // MJ / (kg * K)
}

// [[Rcpp::export]]
double lambda_v(double t_surface){
  // calor latente de vaporización
  // Shutteworth (1992)
  
  double l_v = 2.501 - 0.00236 * t_surface;
  
  return l_v ; // MJ / kg
}

// [[Rcpp::export]]
double m_water(){
  // masa del agua por mol
  return 0.01801528; // [kg / mol] 
}

// [[Rcpp::export]]
double R_gas(){
  // constante de los gases
  return 8.31441; // [J / (mol * K)]
}

// [[Rcpp::export]]
double sigma_stefan(){
  return 5.67 * pow(10, -8); // [W / (m2 * K-4)]
}

// [[Rcpp::export]]
double precip_water(double t_air, //[ºC]
                    double rh){   //[%]
  double t_air_k, W;
  
  t_air_k = t_air + 273.15; // [K]
  
  W = 0.00493 * (rh / t_air_k) * exp(26.23 - 5416 / t_air_k);
  
  return W; // [cm]
}


