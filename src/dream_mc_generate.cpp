#include <Rcpp.h>
using namespace Rcpp;

//' @name dream_mc_generate
//'
//' @title Markov Chain
//'
//' @description Creates a set of \code{K} chains.
//'
//' @param v_param_lwr numeric vector with lower bound parameters.
//'
//' @param v_param_upp numeric vector with upper bound parameters.
//'
//' @param K number of chains to create.
//'
//' @return List with \code{K} initial matrices.


// [[Rcpp::export]]
List dream_mc_generate(NumericVector v_param_lwr,
                       NumericVector v_param_upp,
                       int K){

  int n_p = v_param_lwr.size();
  NumericMatrix m(K, n_p);

  List mc(K);


  for(int i = 0; i < n_p; i++){

    m(_, i) =
      Rcpp::runif(K,
      v_param_lwr(i),
      v_param_upp(i) );

  }


  for(int j = 0; j < K; j++){

    NumericMatrix m_aux = m(Range(j, j), _);
    mc(j) = m_aux;

  }

  return(mc);
}
