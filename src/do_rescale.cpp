#include <Rcpp.h>
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_rescale
//'
//' @title Rescale function
//'
//' @description Allows you to rescale the zone outputs (from
//' \code{do_xx} routines) into a single response using relative
//' areas.
//'
//' @usage do_rescale(
//'        x,
//'        area
//'        )
//'
//' @param x list output coming from \code{do_xx} routines.
//'
//' @param area numeric vector with relative area of every
//' zone (\code{length = number of zones = matrix columns}).
//'
//' @return Numeric matrix with \code{do_xx} routine variables
//' scaled according to \code{area} argument.
//'
//' @export
//'
// [[Rcpp::export]]
NumericMatrix do_rescale(List x,
                         NumericVector area){
  // obtengo nombres de la lista
  CharacterVector nv = x.names();
  int n_it = nv.length();

  // matriz de salida
  NumericMatrix m_auxiliar = x[0];
  int n_rows = m_auxiliar.nrow();
  NumericMatrix m_output(n_rows, n_it);

  for(int i = 0; i < n_it; ++i){
    String ch_id = nv(i); // nombre elemento i lista

    // extraigo matriz
    NumericMatrix m_routine = x[ch_id]; // variable

    // reescalo
    int j_it = area.length();
    NumericMatrix m_rescale(n_rows, j_it);

    for(int j = 0; j < j_it; ++j){
      m_rescale(_, j) = m_routine(_, j) * area(j);
    }

    // obtengo vector de la variable
    m_output(_, i) = rowSums(m_rescale);


  } // for i

  colnames(m_output) = nv;
  return m_output;

}


