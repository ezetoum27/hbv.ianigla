#include <Rcpp.h>
using namespace Rcpp;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Author       : Ezequiel Toum
//  Licence      : GPL V3
//  Institution  : IANIGLA-CONICET
//  e-mail       : etoum@mendoza-conicet.gob.ar
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HBV.IANIGLA package is distributed in the hope that it
//  will be useful but WITHOUT ANY WARRANTY.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//' @name do_tair_gradient
//'
//' @title Air temperature extrapolation model
//'
//' @description Computes air temperature using an extrapolation
//' linear model.
//'
//' @usage do_tair_gradient(
//'        v_tair,
//'        station_alt,
//'        v_altitude,
//'        v_grad
//'        )
//'
//' @param v_tair numeric vector with air temperatures series
//' \eqn{[°C]}.
//'
//' @param station_alt altitude of the air temperature station
//' \eqn{[masl]}
//'
//' @param v_altitude numeric vector with target altitudes
//' \eqn{[masl]} (\code{length = number of zones}).
//'
//' @param v_grad numeric vector with the following elements:
//' \enumerate{
//'
//'   \item air temperature linear gradient [ºC/km].
//'   E.g.: \eqn{-6.5}.
//'
//'   \item threshold height. Air temperature does not decrease
//'   when the altitude is higher than this value \eqn{[masl]}.
//' }
//'
//' @return List with extrapolated air temperatures
//' numeric matrix (\code{ncol = number of zones}).
//'
//' @export
//'
// [[Rcpp::export]]
List do_tair_gradient(NumericVector v_tair,
                      double station_alt,
                      NumericVector v_altitude,
                      NumericVector v_grad){

  int n_row = v_tair.size();
  int n_col = v_altitude.size();

  NumericMatrix m_out(n_row, n_col);

  double grad_tair  = v_grad(0);
  double grad_thres = v_grad(1); // altitudinal threshold

  for(int i = 0; i < n_col; ++i){

    for(int j = 0; j < n_row; ++j){

     if(v_altitude(i) < grad_thres){

        m_out(j, i) = (v_altitude(i) - station_alt) * (grad_tair / 1000) +
                      v_tair(j);

      } else {

        m_out(j, i) = (grad_thres - station_alt) * (grad_tair / 1000) +
                       v_tair(j);
      }


    }




  }

  // salida
  List tair_out =
    List::create(Named("tair") = m_out);

  return tair_out;





}
