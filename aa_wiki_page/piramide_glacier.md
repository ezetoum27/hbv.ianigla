Point ablation simulation in a debris-covered glacier
================
Ezequiel Toum

## What are we going to do?

In this example I will show you how to use the **SnowGlacier\_HBV**
module in a point ablation modeling excercise. After completing it, is
expected that you get a full understanding on how to work with
debris-covered ablation simulations.

## Motivation for debris-covered glacier modeling

Debris-covered glaciers are a common glacier-type in the Central Andes
of Argentina and Chile (∼ 31° S to 35° S). In Mendoza province,
Argentina, 14% ( ∼ 178 km 2 ) of the total glacier surface corresponds
to debris-covered glaciers (IANIGLA-ING, 2018a). Consequently, their
contribution to runoff is highly relevant and could be considered in
regional hydrological simulations. In this section, detailed point field
ablation measurements in a debris-covered glacier are used to test the
suitability of the **SnowGlacier\_HBV** module to simulate the ablation
process at point scale.

![](images/tunuyan.png
"Panoramic view of the Tunuyán debris-covered glacier tongue, Mendoza-Argentina. Photo: Laura Zalazar.")

The debris-cover thickness varies between glaciers and even between
sectors in the same glacier. Depending on the thickness, the debris
coverage can increase or decrease the melting rate. Thin layers of
debris increase the albedo of the ice surface accelerating the ice
melting. In contrast, debris-covered patches or sectors of about 15 cm
or ticker act as a heat- insulator, reducing the melting rates (Mihalcea
et al., 2008; Lambrecht et al., 2011). From a modelling perspective,
this heighten the ‘free-scale‘ modelling approach, i.e., the operator
should be able to freely discretize the surface of a debris-covered
glacier.

## Case study: the Pirámide glacier

As a case study the Pirámide glacier point-ablation model is presented,
an avalanche-fed, debris-covered glacier located in the Yeso river
basin, Chile (33.57° S; 69.88° W). This glacier presents a south aspect,
a surface of about 4.4 km<sup>2</sup> , and a mean altitude around 3670
m, ranging from 3230 to 4593 masl. During the summer seasons 2013-14 /
2014-15, a consulting team measured the ablation rates of the glacier.
Ablation measurements were based on stakes in the ice, automatic weather
stations and air-temperature loggers (Ayala et al., 2016). In this
example, the 2013-14 ablation measurement for the *PI6* stake was used.
Since no precipitation occurred during the campaigns, the glacier
surface was snow-free during the field work.

## Data set

just brief description and to load it from the package

## References

manual citing….fuck\!
