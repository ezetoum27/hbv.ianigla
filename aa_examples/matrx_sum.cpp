#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericMatrix matrix_col_sum(NumericMatrix m1,
                             NumericMatrix m2) {

  int n_it = m1.ncol();

  CharacterVector nms = colnames(m1);

  NumericMatrix m_out(m1.nrow(), n_it);

  for(int i = 0; i < n_it; i++){
    m_out(_, i) = m1(_, i) + m2(_, i);
  }

  colnames(m_out) = nms;

  return m_out;
}

// [[Rcpp::export]]
NumericVector matrix_col_sum2(NumericMatrix m1,
                              NumericMatrix m2){

  NumericVector v1 = as<NumericVector>(m1);
  NumericVector v2 = as<NumericVector>(m2);

  NumericVector vout = v1 + v2;

  vout.attr("dim") = Dimension(m1.nrow(), m1.ncol());

  return vout;

}


