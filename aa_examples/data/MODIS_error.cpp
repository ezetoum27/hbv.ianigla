#include <Rcpp.h>
using namespace Rcpp;

//##########################################################
//#           FUNCIÓN ERROR SIMULACIÓN EAN 
//##########################################################
// DESCRIPCIÓN: esta función cuenta el número de días en que mi modelo sobrestima y subestima 
// el EAN simulado comparando los resultados con series de cobertura nívea MODIS
// REFERENCIA: The value of MODIS snow cover data in validating and calibrating conceptual hydrologic
// models. Parajka and Blöschl (2008)
 
// ARGUMENTOS DE ENTRADA
// SWE  : serie de equivalente agua nieve 
// SCA  : serie de cobertura nívea (admite NA's)
// over : umbral de sobrestimación
// under: umbral de subestimación
 
// SALIDA
// Vector con c(over, under)

// [[Rcpp::export]]
NumericVector MODIS_error_cpp(NumericVector SWE, NumericVector SCA, double over, double under) {
  int n = SCA.size();
  NumericVector out(2);
  
  // Declaro variables
  NumericVector se_over(n), se_under(n);
  double sum_over, sum_under;
  
  // Comienzo bucle
  for (int i = 0; i < n; ++i){
    // Verifico si existe valor en serie SCA
    if ( NumericVector::is_na(SCA[i]) ) {
      se_over[i]  = NA_REAL;
      se_under[i] = NA_REAL;
      
    } else {
    // Condición de sobrestimación
    if (SWE[i] > over & SCA[i] == 0.0) {
      se_over[i] = 1.0;
    } else {
      se_over[i] = 0.0;
    }
    
    // Condición de subestimación
    if (SWE[i] == 0.0 & SCA[i] > under) {
      se_under[i] = 1.0;
    } else {
      se_under[i] = 0.0;
    }
    
    }
  }
  
  // Obtengo la suma de días con error
  sum_over  = sum(se_over);
  sum_under = sum(se_under);
  
  // Salida
  out[0] = sum_over;
  out[1] = sum_under;
  
  return(out);
    
}
  


      

        
