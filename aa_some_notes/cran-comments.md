## New Version
This is a new version. In this version I have:

* Fixed the ERROR issue (flavor r-patched-solaris-x86) related to the ceil() function

## Test environments
* local ubuntu 18.04, R 3.4.4
* win-builder (devel and release)

## R CMD check results
There were no ERRORs,  WARNINGs or NOTEs
